import org.junit.Test;
import static org.junit.Assert.assertEquals;
import com.mycompany.triangularnumbersequence.TriangularNumberSequence;

public class mainTests {

    @Test
    public void test1() {
        assertEquals(1, TriangularNumberSequence.triangle(1));
    }

    @Test
    public void test2() {
        assertEquals(3, TriangularNumberSequence.triangle(2));
    }

    @Test
    public void test3() {
        assertEquals(6, TriangularNumberSequence.triangle(3));
    }

    @Test
    public void test4() {
        assertEquals(36, TriangularNumberSequence.triangle(8));
    }

    @Test
    public void test5() {
        assertEquals(2318781, TriangularNumberSequence.triangle(2153));
    }
}
